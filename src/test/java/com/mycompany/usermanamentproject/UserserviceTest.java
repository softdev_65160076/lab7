/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.usermanamentproject;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class UserserviceTest {
    Userservice userservice;
    public UserserviceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        userservice = new Userservice();
        userservice = new Userservice();
        User newAdmin = new User("Admin","admin", "pass@1234",'M', 'A');
        User newUser1 = new User("User1","user 1", "pass@1234",'M', 'U');
        User newUser2 = new User("User2","user 2", "pass@1234",'M', 'U');
        User newUser3 = new User("User3","user 3", "pass@1234",'M', 'U');
        User newUser4 = new User("User4","user 4", "pass@1234",'M', 'U');
        userservice.addUser(newAdmin);
        userservice.addUser(newUser1);
        userservice.addUser(newUser2);
        userservice.addUser(newUser3);
        userservice.addUser(newUser4);
    }
    
    @AfterEach
    public void tearDown() {
         
    }

    /**
     * Test of addUser method, of class Userservice.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User newUser = new User("administrator","admin", "pass@1234",'M', 'A');
        Userservice instance = new Userservice();
        User expResult = newUser;
        User result = instance.addUser(newUser);
        assertEquals(expResult, result);
        assertEquals(1, result.getId());
    }

    /**
     * Test of getUser method, of class Userservice.
     */
    @Test
    public void testGetUser_int() {
        System.out.println("getUser");
        int index = 1;
        
        String expResult = "user 1";
        User result = userservice.getUser(index);
        assertEquals(expResult, result.getLogin());
  
    }

    /**
     * Test of getUser method, of class Userservice.
     */
    @Test
    public void testGetUser_0args() {
        System.out.println("getUser");
        
        ArrayList<User> userList = userservice.getUser();
        assertEquals(5, userList.size());

    }

    /**
     * Test of getSize method, of class Userservice.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        int expResult = 5;
        int result = userservice.getSize();
        assertEquals(expResult, result);

    }
    
}
