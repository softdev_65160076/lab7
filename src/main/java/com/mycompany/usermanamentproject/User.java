/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanamentproject;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class User implements Serializable{
    private int id;
    private  String name;
    private  String longin;
    private  String password;
    private  char gender;
    private  char role;
    private  static int lassid = 1;

    public User(String name, String longin, String password, char gender, char role) {
        this(User.lassid++,longin,name,password,gender,role);
    }

    

    
    public User(int id, String longin,String name, String password, char gender, char role) {
        this.id = id;
        this.name = name;
        this.longin = longin;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return longin;
    }

    public void setLongin(String longin) {
        this.longin = longin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public char getGender() {
        return gender;
    }
    public String getGenderString(){
        if(gender=='M'){
            return "Male";
        }else{
            return "Female";
        }
    }
    public void setGender(char gender) {
        this.gender = gender;
    }

    public char getRole() {
        return role;
    }
    
    public String getRoleString(){
        if(gender=='A'){
            return "Admin";
        }else{
            return "User";
        }
    }
    public void setRole(char role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", longin=" + longin + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }

    
    
    
    


    
}
